package cn.lrtan.ui;

import javax.swing.*;
import java.awt.*; //只导入当前包中的类
import java.awt.event.*;

public class MyWind extends JFrame implements ActionListener{
	JLabel	label;
	public MyWind(String title) {
		super(title);
		this.setBounds(10,0,400,600);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLayout(null); //BorderLayout
		GameWindow gWindow=new GameWindow();
		gWindow.setBounds(0, 0, 300, 300);
		gWindow.setBackground(Color.black);
		this.getContentPane().add(gWindow);
		this.setVisible(true);
		gWindow.go();
	}
	/**
	 * 初始化控件component
	 */
	void init() {
		Container container=this.getContentPane();
	 label=new JLabel("要在label显示的内容");
		label.setLocation(50,50);
		label.setSize(200, 50);
		container.add(label);
		
		JButton btn=new JButton("确定");
		btn.setBounds(50, 200, 60, 30);
		container.add(btn);
		
		
		
		
		JTextField textField=new JTextField(10);
		textField.setLocation(50,100);
		textField.setSize(100, 50);
		container.add(textField);
		//绑定事件监听器时，事件对象实现的三种方法：
		//事件源.addXXXListener(事件对象);
		//1.内部类来实现
		//2.匿名类
		//3.本实现
		//都要实现ActionListener接口
//		//内部类
//		class BtnActionListener implements ActionListener{
//			 public void actionPerformed(ActionEvent e) {
//				// System.out.println("单击了按钮");
//				String str= textField.getText();
//				label.setText(str);
//			 }
//		}
//		btn.addActionListener(new ActionListener() {
//			public void actionPerformed(ActionEvent e) {
//				Object obj=e.getSource();
//      			JButton jbtnButton=null;//返回事件源
//				if(obj instanceof JButton) {
//					jbtnButton=(JButton)obj;
//					label.setText(jbtnButton.getText()); 
//				}
//				
//			 }
//		});
		JButton btn1=null;
		btn.addActionListener(this);
//		btn1.addActionListener(this);
	}
	public void actionPerformed(ActionEvent e) {
		Object obj=e.getSource();
			JButton jbtnButton=null;//返回事件源
		if(obj instanceof JButton) {
			jbtnButton=(JButton)obj;
			label.setText(jbtnButton.getText()); 
		}
		
		JOptionPane.showMessageDialog(this, "这个是一个消息框！");
		
	 }
	public static void main(String[] args) {
		new MyWind("JframeTest");
		
		//1.创建窗口
		//2.添加组件
		//3.处理单机事件
	}

}


