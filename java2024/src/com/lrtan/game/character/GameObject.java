package com.lrtan.game.character;

public class GameObject  extends Object{
	private String name;
	private int x,y;
	double getDistance(GameObject object) {
		return Math.sqrt(Math.pow(x-object.getX(), 2)+Math.pow(y-object.getY(),2)) ;
	}
	public int getX() {
		return x;
	}
	public int getY() {
		return x;
	}
	
	//重写父类Object中的toString()方法
	//1.子类中重写父类的方法时，修饰范围要等于或大于父类修饰的范围
	//2.要求你子类的方法名、形参、返回值必须与被重写的方法一样。
	@Override
	public String toString() {
		return name+",x="+x+",y="+y;
	}
}
